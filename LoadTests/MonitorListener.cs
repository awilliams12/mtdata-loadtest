﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Net.Http;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using static Newtonsoft.Json.Linq.JToken;

namespace MTData.LoadTest
{
    [TestClass]
    public class MonitorListener
    {
        private static readonly WebClient webClient = new WebClient();

        public class SearchResult
        {
            public string lastvalue { get; set; }
            public string name { get; set; }
            public string lastmessage { get; set; }
        }

        [TestMethod]
        public void HttpTest()
        {
            string auth = "&USERNAME=mtdata.awilliams&PASSWORD=Abcd1234";

            string url = "https://prtg.mtdata.com.au/api/table.json?content=channels&output=json&columns=name,lastvalue_&id=7363";
            //                                        api/table.json?content=values&output=json&columns=datetime,value_,coverage&id=7363&noraw=1&usecaption=true
            string response = webClient.DownloadString(url + auth);
            Console.WriteLine("Http response: " + response);

            url = "https://prtg.mtdata.com.au/CHART.SVG?TYPE=GRAPH&WIDTH=300&HEIGHT=160&GRAPHID=0&hide=-4,0&ID=3219";
            var graph = webClient.DownloadString(url + auth);


            url = "https://prtg.mtdata.com.au/api/GETSENSORDETAILS.JSON?ID=7976";
            response = webClient.DownloadString(url + auth);
            Console.WriteLine("Http response: " + response);

            // Listener.6
            url = "https://prtg.mtdata.com.au/api/GETSENSORDETAILS.JSON?ID=6732";
            response = webClient.DownloadString(url + auth);
            Console.WriteLine("Listener Http response: " + response);

            // Sys Health
            url = "https://prtg.mtdata.com.au/api/GETSENSORDETAILS.JSON?ID=3219";
            response = webClient.DownloadString(url + auth);
            Console.WriteLine(" Sys Health Http response: " + response);

           JObject prtgData = JObject.Parse(response);

            IList<JToken> results = prtgData["sensordata"].Children().ToList();
            string lastvalue = (string)prtgData["sensordata"]["lastvalue"];
            string lastmessage = (string)prtgData["sensordata"]["lastmessage"];
            string name = (string)prtgData["sensordata"]["name"];
            Console.WriteLine(" Sys Health : " + name + "  " + lastvalue + "  " + lastmessage + "  ");

            SearchResult searchResult = prtgData["sensordata"].ToObject<SearchResult>();
        }

        //private void Form1_Paint(object sender, PaintEventArgs e)
        //{
        //    Graphics graphics = e.Graphics;

        //    int width = 60;
        //    int height = 60;

        //    // Create a Bitmap object from a file.
        //    Image sourceImage = Image.FromFile(@"C:\Users\Mike\Downloads\logo.png");

        //    // Draw a portion of the source image.
        //    Rectangle sourceRect = new Rectangle(0, 0, width, height);
        //    graphics.DrawImage(sourceImage, 0, 0, sourceRect, GraphicsUnit.Pixel);
        //}

    }

}

// ASYNC:
// private static readonly HttpClient client = new HttpClient();
// var responseString = async client.GetStringAsync(url + auth);
// Console.WriteLine("Http response: " + responseString);  